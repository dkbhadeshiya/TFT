import socket

soc = socket.socket()

server = raw_input("Enter server address : ") #Asks for server name from user

port = input("Enter Port Name you Want to connect on : ")   #Asks for port of server

soc.connect((server,port))  #Connect to Server running on server ip and port

fileName=soc.recv(1024).decode()    #Recieves sent filename in binary and decodes to string using .decode()

print 'Reciving File ',fileName

f = open (fileName,'wb')    #Opens recieved file name in binary write mode to write
packet = soc.recv(1024) #Recieves packet in binary format
while (packet):
    f.write(packet) 
    packet = soc.recv(1024)
f.close()   #Close file
soc.close() #Close Socket
print 'Done Reciving...'
