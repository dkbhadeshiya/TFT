import socket

soc = socket.socket()

host = ''  #Default Host address


port = input("Enter Port Name you Want your server to listen on : ")    #Asks for port

soc.bind((host,port))   #Bind port and host and create socket

soc.listen(1)   #Listen to socket, 1 Connection at max

print 'Server started on ',host,':',port

fileName = raw_input("Enter the File Name : ")  #Asks for filename from user

f = open(fileName,'rb') #Open file in read mode and binary mode

print 'Waiting for client to connect to send',fileName
while True:
    c, addr = soc.accept()  #Accept connection from client
    print 'Connection Established, Client ID :',addr[0]
    c.send(fileName)    #Send name of the file to client so that it can save it
    packet = 'a'   #Reads 1024 bits from file
    print 'Sending file...'
    while (packet) :
            packet = f.read(1024)
            c.send(packet)  #Sends 1024 bits to socket in loop
    f.close()   #Close the file
    c.shutdown(socket.SHUT_WR)  #Notifies the remote socket that sending is finished
    c.close()   #Close connection
    print 'File Transmitted'
